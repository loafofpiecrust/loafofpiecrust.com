import React from "react"
import { Layout } from "components/layout"

export default () => (
  <Layout title="Not Found">
    <div style={{ textAlign: "center" }}>
      <h1>404</h1>
      <hr />
      <h3>This page does not exist. You are dreaming.</h3>
      <h3>Continue on your way, traveller.</h3>
      <h3>There is nothing to see here.</h3>
    </div>
  </Layout>
)
